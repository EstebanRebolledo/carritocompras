/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author DivergentSoul
 */
public class EmpleadoDAOTest {
    
    public EmpleadoDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validar method, of class EmpleadoDAO.
     */
    @Test
    public void testValidar_CaseExiste() {
        System.out.println("validar");
        String user = "felper";
        String Rut = "20327985-9";
        EmpleadoDAO instance = new EmpleadoDAO();
        String expResult = "Felipe Peralta";
        Empleado result = instance.validar(user, Rut);
        assertTrue("El no empleado existe", (expResult.equals(result.getNom())));
    }
    /**
     * Test of validar method, of class EmpleadoDAO.
     */
    @Test
    public void testValidar_CaseNoExiste() {
        System.out.println("validar");
        String user = "felpe";
        String Rut = "20327985-9";
        EmpleadoDAO instance = new EmpleadoDAO();
        String expResult = "Felipe Peralta";
        Empleado result = instance.validar(user, Rut);
        assertFalse("asd",(expResult.equals(result.getNom())));
    }



    /**
     * Test of agregar method, of class EmpleadoDAO.
     */
    @Test
    public void testAgregar() {
        //Creamos Empleado y lo agregamos
        System.out.println("agregar");
        Empleado em = new Empleado( 9999,"18152961-K", "Esteban Rebolledo", "82447574", "Inactivo", "estreb");
        EmpleadoDAO instance = new EmpleadoDAO();
        String expResult = "Esteban Rebolledo";
        int result = instance.agregar(em);
        if(result != 1){
            fail("fallo al agregar a la base de datos");
        }
        //Revisamos que exista
        EmpleadoDAO checkInstance = new EmpleadoDAO();
        Empleado checkResult = checkInstance.validar("estreb", "18152961-K");
        //Comprobamos que tome los datos
        assertTrue("El no empleado existe", (expResult.equals(checkResult.getNom())));
        // Borramos al sujeto de prueba
        checkInstance.delete(checkResult.getId());
    }




    /**
     * Test of delete method, of class EmpleadoDAO.
     */
    @Test
    public void testDelete() {
        //Creamos Empleado y lo agregamos
        System.out.println("Delete");
        Empleado em = new Empleado( 9999,"19164984-2", "Catalina Suárez", "82447574", "Inactivo", "catsua");
        EmpleadoDAO instance = new EmpleadoDAO();
        String expResult = "Catalina Suárez";
        int result = instance.agregar(em);
        if(result != 1){
            fail("fallo al agregar a la base de datos");
        }
        //Obtenemos el id
        EmpleadoDAO checkInstance = new EmpleadoDAO();
        Empleado checkResult = checkInstance.validar("catsua", "19164984-2");
        // Borramos al sujeto de prueba
        checkInstance.delete(checkResult.getId());
        //Revisamos que exista
        checkInstance = new EmpleadoDAO();
        checkResult = checkInstance.validar("catsua", "19164984-2");
        
        //Comprobamos que tome los datos
        assertFalse("El no fue borrado", (expResult.equals(checkResult.getNom())));
    }
    
    /**
     * Test of listar method, of class EmpleadoDAO.
     */
    @Test
    public void testListar() {
        System.out.println("listar");
        EmpleadoDAO instance = new EmpleadoDAO();
        List<String> expResult = Arrays.asList("felper", "EstReb");
        List<Empleado>result= instance.listar();
        for (int i = 0; i < result.size(); i++) {
            if (!result.get(i).user.equals(expResult.get(i))){
                fail("El usuario "+result.get(i).nom+" no coincidió");
            }
        }
    }
    
    /**
     * Test of actualizar method, of class EmpleadoDAO.
     */
    @Test
    public void testActualizar_actualizarNombre() {
        System.out.println("actualizar");
        EmpleadoDAO instance = new EmpleadoDAO();
        Empleado instancia = instance.validar("felper", "20327985-9");
        System.out.println(instancia.getNom());
        instancia.setNom("Catalina Suárez");
        System.out.println(instancia.getNom());
        instance.actualizar(instancia);
        Empleado nuevaInstancia = instance.validar("felper", "20327985-9");
        System.out.println(nuevaInstancia.getNom());
        assertTrue("El usuario no cambió de nombre", nuevaInstancia.getNom().equals("Catalina Suárez"));
        nuevaInstancia.setNom("Felipe Peralta");
        instance.actualizar(nuevaInstancia);
    }
    
    /**
     * Test of listarId method, of class EmpleadoDAO.
     */
    @Test
    public void testListarId_comprobarQueEncuentra() {
        System.out.println("listarId");
        int id = 1;
        EmpleadoDAO instance = new EmpleadoDAO();
        Empleado expResult = null;
        Empleado result = instance.listarId(id);
        if (result==null){
            fail("El empleado no fue encontrado y debería haber sido encontrado");
        }
        //carrito
    }
    /**
     * Test of listarId method, of class EmpleadoDAO.
     */
    @Test
    public void testListarId_comprobarEncontrarAlCorrecto() {
        System.out.println("listarId");
        int id = 1;
        EmpleadoDAO instance = new EmpleadoDAO();
        Empleado expResult = null;
        Empleado result = instance.listarId(id);
        if (!result.user.equals("felper")){
            fail("El empleado encontrado no corresponde al id");
        }
        //carrito 
    }

}
