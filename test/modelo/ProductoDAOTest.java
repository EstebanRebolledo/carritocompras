/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import com.oracle.nio.BufferSecrets;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sun.security.pkcs11.P11Util;

/**
 *
 * @author ninja
 */
public class ProductoDAOTest {
    
    public ProductoDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of buscar method, of class ProductoDAO.
     */
    @Test
    public void testBuscarNoExiste() {
        System.out.println("buscar No existe");
        int id = 1;
        ProductoDAO instance = new ProductoDAO();
        int expResult = 0;
        Producto result = instance.buscar(id);
        assertEquals(expResult, result.getId());
        if (result.getId() == 1){
        fail("producto existe");
        }
        // TODO review the generated test code and remove the default call to fail.

    }
    
    @Test
    public void testBuscarExiste() {
        System.out.println("buscar existe");
        int id = 7;
        ProductoDAO instance = new ProductoDAO();
        int expResult = 7;
        Producto result = instance.buscar(id);
        assertEquals(expResult, result.getId());
        if (result.getId() != 7){
        fail("producto no existe");
        }
    }

    /**
     * Test of actualizarStock method, of class ProductoDAO.
     */
    @Test
    public void testActualizarStock() {
        System.out.println("actualizarStock");
        int id =7;
        int stock =12;
        ProductoDAO instance = new ProductoDAO();
        int expResult =12;
        instance.actualizarStock(id, stock);
        Producto result = instance.buscar(id);
        assertEquals(expResult, result.stock);
        // TODO review the generated test code and remove the default call to fail.
        if (result.stock != 12) {
            fail("error, se esperaba 12");
        }
        instance.actualizarStock(id, 10);
    }
        

    /**
     * Test of listarId method, of class ProductoDAO.
     */
    @Test
    public void testListarId() {
        System.out.println("listarId");
        int id = 7;
        ProductoDAO instance = new ProductoDAO();
        int expResult = 7;
        Producto result = instance.buscar(id);
        assertEquals(expResult, result.getId());
        if (result.getId() != 7){
        fail("producto no existe");
        }
    }
    
    
    /**
     * Test of listar method, of class ProductoDAO.
     */
    @Test
    public void testListar() {
        System.out.println("listar");
        ProductoDAO instance = new ProductoDAO();
        List<Producto> expResult = new ArrayList<Producto>();
        List<Producto> result = instance.listar();
        //generar resultado esperado
        Producto p1 = new Producto(7,"papas de jamon", null, "papas fritas envasadas",99999999, 10);
        Producto p2 = new Producto(8,"Producto 2", null, "Este es el producto 2",12800,5);
        Producto p3 = new Producto(9,"Producto 3",null,"Este es el producto 3",35900,9);
        Producto p4 = new Producto(10,"Producto 4",null,"Este es el producto 4",990,4);
        Producto p5 = new Producto(11,"Producto 5",null,"Este es el producto 5",45900,2);
        Producto p6 = new Producto(12,"Producto 6",null,"Este es el producto 6",23890, 6);
        expResult.add(p1);
        expResult.add(p2);
        expResult.add(p3);
        expResult.add(p4);
        expResult.add(p5);
        expResult.add(p6);
        System.out.println("List"+ expResult);
        System.out.println("List"+ result);
        for( int i =0;i<=5;i++){
            /*
            System.out.println(result.get(i).nombres);
            System.out.println(expResult.get(i).nombres);
            System.out.println(result.get(i).descripcion);
            System.out.println(expResult.get(i).descripcion);
            System.out.println(result.get(i).precio);
            System.out.println(expResult.get(i).precio);
            */
            assertTrue("No lista correctamente", result.get(i).nombres.equals(expResult.get(i).nombres) );
            assertTrue("No lista correctamente", result.get(i).descripcion.equals(expResult.get(i).descripcion) );
            assertTrue("No lista correctamente", result.get(i).precio == expResult.get(i).precio);
        }

    }

    /**
     * Test of listarImg method, of class ProductoDAO.
     */
    /*
    @Test
    public void testListarImg() {
        System.out.println("listarImg");
        int id = 0;
        HttpServletResponse response = null;
        ProductoDAO instance = new ProductoDAO();
        instance.listarImg(id, response);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    */

    /**
     * Test of agregar method, of class ProductoDAO.
     */
    @Test
    /** agregar producto**/
    public void testAgregar() {
        System.out.println("agregar");
        Producto p = new Producto(999, "producto 8", null, "este es el producto 7", 8750, 5);
        ProductoDAO instance = new ProductoDAO();
        int expResult = 1;
        int result = instance.agregar(p);
        assertTrue(" error, no se agregó - assert",expResult == result);
        // TODO review the generated test code and remove the default call to fail.
        if(result != 1){
            fail("error no se agrego el producto");
        }   
        instance.delete(999);
        
    }    
    /** agregar produco ya existente **/
    @Test
    public void testAgregarExistente() {
        System.out.println("agregar");
        Producto p = new Producto(7, "producto 8", null, "este es el producto 7", 8750, 5);
        ProductoDAO instance = new ProductoDAO();
        int expResult = 0;
        int result = instance.agregar(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(result == 1){
            fail("el producto se agrega");
            instance.delete(p.getId());
        }        
    }   
    
    /**
     * Test of actualizar method, of class ProductoDAO.
     */
    @Test
    public void testActualizar() {
        System.out.println("actualizar");
        Producto p = new Producto(7, "papas de jamon", null, "papas fritas envasadas", 99999999, 10);
        ProductoDAO instance = new ProductoDAO();
        int expResult = 1;
        int result = instance.actualizar(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(result != 1){
        fail("error, producto no actualizado");
        }     
    }

    /**
     * Test of delete method, of class ProductoDAO.
     */
    @Test
    public void testDelete(){
    System.out.println("delete");    
    //creamos un producto    
        Producto p = new Producto(25,"producto 25",null, "este es el producto 25",100,5);
        ProductoDAO instance = new ProductoDAO();
        int expResult = 1;
        int result = instance.agregar(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(result != 1){
            fail("el producto no se agrega");
            
        } 
        
        ProductoDAO checkInstance = new ProductoDAO();
        Producto checkResult = checkInstance.buscar(25);
        // Borramos al sujeto de prueba
        checkInstance.delete(checkResult.id);
        //Revisamos que exista
        checkInstance = new ProductoDAO();
        checkResult = checkInstance.buscar(25);
        
        //Comprobamos que tome los datos
        if (checkResult.id != 0){
            fail("producto no eliminado");
        }
        
    }
    
}